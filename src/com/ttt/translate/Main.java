package com.ttt.translate;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Main extends JFrame{

    private static JComboBox comboBox;
    private JTextArea textArea_1;
    private JComboBox comboBox_1;
    private Boolean isload;

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        //允许设置给jframe设置皮肤
        JFrame.setDefaultLookAndFeelDecorated(true);
        //设置皮肤
        UIManager.setLookAndFeel("ch.randelshofer.quaqua.QuaquaLookAndFeel");
        //设置主界面的属性
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    //创建对象本身 LoginInterface
                    Main frame = new Main();
                    //设置窗口可见;
                    frame.setVisible(true);
                    //设置窗口居中显示
                    frame.setLocationRelativeTo(null);
                    //设置窗口不能被放大
                    frame.setResizable(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //静态get--JcomboBox方法
    public static JComboBox getComboBox() {
        return comboBox;
    }


    //绘制主界面窗口
    public Main() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 500, 329);
        getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 500, 309);
        getContentPane().add(panel);
        panel.setLayout(null);

        JTextArea textArea = new JTextArea();
        textArea.setBounds(0, 0, 500, 114);
        textArea.setLineWrap(true);        //激活自动换行功能
        textArea.setWrapStyleWord(true);            // 激活断行不断字功能
        //字体样式
        textArea.setFont(new Font(textArea.getFont().getName(),textArea.getFont().getStyle(),16));
        panel.add(textArea);
        //快捷键事件
        textArea.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER){
                    //获取翻译的字符
                    String text = textArea.getText();
                    //进行翻译
                    TranslateDemo translateDemo = new TranslateDemo();
                    String translate = translateDemo.translate(text);
                    //将翻译的值填充
                    textArea_1.setText(translate);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        textArea_1 = new JTextArea();
        textArea_1.setBounds(0, 155, 500, 156);
        textArea_1.setLineWrap(true);        //激活自动换行功能
        textArea_1.setWrapStyleWord(true);            // 激活断行不断字功能
        //字体样式
        textArea_1.setFont(new Font(textArea_1.getFont().getName(),textArea_1.getFont().getStyle(),16));
        panel.add(textArea_1);

        comboBox = new JComboBox();
        comboBox.setBounds(190, 117, 120, 35);
        comboBox.addItem("English");
        comboBox.addItem("Chinese");
        comboBox.addItem("Wyw");
        comboBox.addItem("Jp");
        panel.add(comboBox);

        comboBox_1 = new JComboBox();
        comboBox_1.setBounds(10, 117, 120, 35);
        comboBox_1.addItem("Auto");
        comboBox_1.addItem("Chinese");
        comboBox_1.addItem("English");
        panel.add(comboBox_1);

        JButton btnNewButton = new JButton("翻译");
        btnNewButton.setBounds(370, 117, 100, 35);
        panel.add(btnNewButton);
        isload = true;
        //点击事件
        btnNewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(111);
                btnNewButton.setText("loading…");
                try {
                    if (isload){
                        System.out.println(222);
                        isload = false;
                        //获取翻译的字符
                        String text = textArea.getText();
                        if (text.equals("")){
                            System.out.println("输入为空");
                            //将翻译的值填充
                            textArea_1.setText("请输入要翻译的内容！");
                        }else {
                            //进行翻译
                            TranslateDemo translateDemo = new TranslateDemo();
                            String translate = translateDemo.translate(text);
                            //将翻译的值填充
                            textArea_1.setText(translate);
                        }
                    }else {
                        System.out.println(2222);
                        System.out.println("qingqiuzhong");
                    }

                }finally {
                    System.out.println(3333);
                    btnNewButton.setText("翻译");
                    //isload = false;
                }
                isload = true;
            }
        });

        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("img\\ziliao.png"));
        lblNewLabel.setBounds(140, 117, 55, 35);
        panel.add(lblNewLabel);
    }

}
