package com.ttt.translate;

import static com.ttt.translate.Main.getComboBox;
import static com.ttt.translate.UnicodeToUtf8Impl.UnicodeToUtf8;
//翻译主方法
public class TranslateDemo {

    // 在平台申请的APP_ID 详见 http://api.fanyi.baidu.com/api/trans/product/desktop?req=developer
    private static final String APP_ID = "20200713000518552";
    private static final String SECURITY_KEY = "9GVGaN2cXJrKMJCudDOc";

    public String translate(String needtranstext) {

        //获取界面的下拉控件的当前值控件
        String selected_value = getComboBox().getSelectedItem().toString();
        //将获得值进行转化
        switch (selected_value){
            case "Chinese":
                selected_value = "zh";
                break;
            case "English":
                selected_value = "en";
                break;
            case "Wyw":
                selected_value = "wyw";
                break;
            case "Jp":
                selected_value = "jp";
                break;
            default:
                break;
        }
        TransApi api = new TransApi(APP_ID, SECURITY_KEY);
        //获取翻译结果
        String transResult = api.getTransResult(needtranstext, "auto", selected_value);
        System.out.println(transResult);
        //截取字符串
        String dst = transResult.substring((transResult.indexOf("dst") + 6), (transResult.length() - 4));
        //unicode转换中文
        String s = UnicodeToUtf8(dst);
        //返回结果
        return s;
    }
}
